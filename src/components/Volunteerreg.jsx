import React from 'react'

class Volunteerreg extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            'fname' : '',
            'phone': '',
            'email': '',
            'location': '',
            'occupation': ''
        };
    }

    handleChange = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
    }

    submitHandler = (event) => {
        event.preventDefault();
        alert(this.state);
    }
    render(){
        const form = (
            <div className="row">
                <div className="col-md-4"></div>
                <form onSubmit = {this.submitHandler} className="text-center col-md-4">
                        <img src="" alt="Logo"/>
                        <h3>Register as Volunteer</h3>
                        <br/>
                        <input className="form-control" type="text" placeholder='Full Name' name="fname" value={this.state.fname} onChange={this.handleChange}/>
                        <br/>
                        <input className="form-control" type="text" placeholder='Phone' name="phone" value={this.state.phone} onChange={this.handleChange}/>
                        <br/>
                        <input className="form-control" type="text" placeholder='Email' name="email" value={this.state.email} onChange={this.handleChange}/>
                        <br/>
                        <input className="form-control" type="text" placeholder='Location' name="location" value={this.state.location} onChange={this.handleChange}/>
                        <br/>
                        <input className="form-control" type="text" placeholder='Occupation' name="occupation" value={this.state.occupation} onChange={this.handleChange}/>
                    <br/>
                        <input  className=" col-6 form-control btn btn-success" type="submit" value="Register" />
                </form>
                <div className="col-md-4"></div>
            </div>
        );

        return form;
    }
}

export default Volunteerreg;
