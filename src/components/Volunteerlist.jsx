import React from 'react';
import "./volunteerlist.css"
class Volunteerlist extends React.Component{
    state = {
        persons: [],
        search: ''
      }
      componentDidMount() {
        fetch('http://jsonplaceholder.typicode.com/users')
        .then(res => res.json())
        .then((data) => {
          this.setState({ persons: data })
        })
        .catch(console.log)
      }

      handleChange = (event) => {
          let nam = event.target.name;
          let val = event.target.value;
          this.setState({[nam]: val});
      }

      searchSubmitHandler = (event) => {
          event.preventDefault();
      }

      sortSubmitHandler = (event) => {
          event.preventDefault();
      }


  render () {

      // JSX to render goes here...
      const persons = this.state.persons.map((person) => (
              <tr className="row" key={person.id}>
                  <td className="col-3">{person.name}</td>
                  <td className="col-2">{person.address.city}</td>
                  <td className="col-2">{person.company.bs}</td>
                  <td className="col-2">{person.phone}</td>
                  <td className="col-3">{person.email}</td>
            </tr>
          ));

          var headStyle = {
              background: "#000",
              color: "#fff"
          };


    return (
        <div className="container">
            <div className="row" style={headStyle}>
                <div className="col-12">
                    <br/>
                </div>
                <div className="col-2">
                    <img src="" alt="Logo"/>
                </div>

                <div className="col-4">
                    <h2>Volunteer Details</h2>
                </div>
                <div className="col-4">
                    <form onSubmit = {this.searchSubmitHandler}>
                        <label callName="form-group">
                            <input type="text" className="form-control" value={this.state.search} name="search" onChange={this.handleChange}/>
                        </label>
                        &nbsp;
                        &nbsp;
                        <label callName="form-group">
                            <button className="form-control"><i class="fa fa-search"></i></button>
                        </label>
                    </form>
                </div>
                <div className="col-2">
                    <form onSubmit = {this.sortSubmitHandler}>
                        <select name="" id="" className="form-control">
                            <option value=""> - Sort by - </option>
                            <option value="location">Location</option>
                            <option value="occupation">Occupation</option>
                        </select>
                    </form>
                </div>
                <div className="col-12">
                    <br/>
                </div>
            </div>
            <table className="container table table-hover">
                <thead className="row thead-dark">
                    <th className="col-3">Volunteer Name</th>
                    <th className="col-2">Location</th>
                    <th className="col-2">Occupation</th>
                    <th className="col-2">Phone</th>
                    <th className="col-3">Email</th>
                </thead>
                {persons}
            </table>
        </div>
    );
  }
}

export default Volunteerlist;
