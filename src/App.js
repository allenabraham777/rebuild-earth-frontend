import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

// pages
import Home from './pages/Home'
import Login from './pages/Login'
import Volunteerreg from './components/Volunteerreg';
import Volunteerlist from './components/Volunteerlist';

function App() {
  return (
    <div className="App">
      <Router>
      <div>
        <p>
        <Link to="/">Home</Link>
        <br/>
        <Link to="/login">Login</Link>
        <br/>
        <Link to="/volunteer/register">Volunteer</Link>
        <br/>
        <Link to="/volunteer/search">Volunteer Search</Link>
        </p>
        {/*
            Add your routes here
            the first matched route will be used
        */}
        <Switch>

          <Route path="/login" exact >
            <Login />
          </Route>
          <Route path="/volunteer/register" exact >
              <Volunteerreg />
          </Route>
          <Route path="/volunteer/search" exact >
              <Volunteerlist />
          </Route>

          <Route path="/" component="">
            <Home />
          </Route>

        </Switch>
      </div>
    </Router>
    </div>
  );
}

export default App;
